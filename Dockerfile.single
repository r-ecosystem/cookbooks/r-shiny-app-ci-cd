FROM r-base:latest

LABEL maintainer "Jean-Francois Rey <jean-francois.rey@inra.fr>"

## Add here libraries dependencies
## there are more libraries that are needed here
RUN apt-get update && apt-get install -y \
    sudo \
    gdebi-core \
    pandoc \
    pandoc-citeproc \
    libcurl4-gnutls-dev \
    libcairo2-dev/unstable \
    libxt-dev \
    libssl-dev \
    libpq-dev \
    libgeos-dev \
    locales \
    libproj-dev \
    libgdal-dev gdal-bin \
    libudunits2-0 libudunits2-data libudunits2-dev \
    git \
    openssh-client \
    libssh2-1-dev \
    libgit2-dev \
    libglib2.0-dev/unstable \
    libgsl-dev \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

## Change timezone
ENV CONTAINER_TIMEZONE Europe/Paris
ENV TZ Europe/Paris

RUN sudo echo "Europe/Paris" > /etc/timezone
RUN echo "fr_FR.UTF-8 UTF-8" >> /etc/locale.gen \
  && locale-gen fr_FR.UTF8 \
  && /usr/sbin/update-locale LANG=fr_FR.UTF-8

ENV LC_ALL fr_FR.UTF-8
ENV LANG fr_FR.UTF-8

## R package dependencies
## Add here R packages dependencies
RUN Rscript -e "install.packages(c('shiny'), repos='https://cran.biotools.fr/')"
## To get last version of packages
# comment it if you install specific R packages version
RUN Rscript -e "update.packages(ask=FALSE)"

## App name by default, the name is set at docker build dynamically using project name
ARG APP_NAME="superApp"


## Clean unnecessary libraries
RUN apt-get update && apt-get remove --purge -y \
    gdebi-core \
    libcurl4-gnutls-dev \
    libcairo2-dev/unstable \
    libxt-dev \
    libssl-dev \
    libpq-dev \
    libgeos-dev \
    libproj-dev \
    libgdal-dev \
    libudunits2-dev \
    git \
    openssh-client \
    libssh2-1-dev \
    libgit2-dev \
  && apt-get autoremove -y \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

## Change shiny user rights
## USER shiny
RUN passwd shiny -d
WORKDIR /home/shiny

## Copy the application into image
## Add here files and directory necessary for the app.
## global.R ui.R server.R ...
RUN mkdir -p /home/shiny//${APP_NAME}
COPY ui.R /home/shiny/${APP_NAME}
COPY server.R /home/shiny/${APP_NAME}
#COPY global.R /home/shiny/${APP_NAME}
COPY www/* /home/shiny/${APP_NAME}/www/
# Copy App directory
#COPY myApp /home/shiny/${APP_NAME}

USER shiny

EXPOSE 3838

CMD ["Rscript","-e","shiny::runApp('/home/shiny/${APP_NAME}', port = 3838)"]


