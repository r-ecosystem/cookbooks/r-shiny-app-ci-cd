[![pipeline status](https://gitlab.paca.inra.fr/r-ecosystem/cookbooks/r-shiny-app-ci-cd/badges/master/pipeline.svg)](https://gitlab.paca.inra.fr/r-ecosystem/cookbooks/r-shiny-app-ci-cd/commits/master)

> GitLab, R, R Shiny, Docker, Dockerfile, ShinyProxy

# R Shiny Continuous Integration and Deployment (CI/CD) pipeline

This project hosts the repository of a Cookbook to create Docker image of a R Shiny
App and to deploy it in production.  

This template builds a Docker image of the R Shiny App using a __Dockerfile__ 
and deploys it to a server. All tasks (jobs) are set in the __.gitlab-ci.yml__ file.

## Have a quick look at the poster presentation : [hal-02899373](https://hal.archives-ouvertes.fr/hal-02899373)


**Table of Contents**

- [Prerequisites](#prerequisites)
- [Files](#files)
- [Principle](#principle)
- [Set GitLab](#set-gitlab)
  - [The Project](#the-project-repository)
  - [The Runners](#the-runners)
- [The .gitlab-ci.yml file](#the-gitlab-ciyml-file)
- [Dockerfiles](#dockerfiles)
- [Test the docker image](#test-the-docker-image)
- [Author](#author)
- [License](#license)


## Prerequisites

* Have to know:
  * GitLab project management
  * R Shiny App   

* Should to know :
  * GitLab Runners principle
  * GitLab CI/CD principle
  * Docker principle

## Files

* __[ui.R](ui.R)__, __[server.R](server.R)__ and __[www/](www)__ : R Shiny App files and directory
* __[.gitlab-ci.yml](.gitlab-ci.yml)__ : file configuring the CI/CD pipeline
* __[Dockerfile.multi](Dockerfile.multi)__ : file to build a Docker image of the App with shiny server (multi-users)
* __[Dockerfile.single](Dockerfile.single)__ : file to build a Docker image of the App (single-user)
* __[LICENSE](LICENSE)__ : the license file
* __[[myApp-compose.yml]]__ and __[myApp-stack.yml]]__ : specific deployment files for docker.

## Principle

![Gitlab CI/CD R Shiny App pipeline](www/GitLab_CI_CD_pipeline_RShinyApp.png)

* At push, in GitLab repository, the pipeline is trigger.
* GitLab use __[.gitlab-ci.yml](.gitlab-ci.yml)__ file to set the pipeline and run it on runners.
  * First it builds the docker image using __[Dockerfile](Dockerfile.multi)__
  * Then it pushs it in the container registry of the project (with commit ID and _latest_ tags)
  * At the end it deploys the image in a server (with a docker environment)

## Set GitLab

### The project repository

You can fork this project and use it as a template.  

* Files in the project:  
  * __[ui.R](ui.R)__, __[server.R](server.R)__ and __[www/](www)__ : R Shiny App files and directory
  * __[.gitlab-ci.yml](.gitlab-ci.yml)__ : file configuring the CI/CD pipeline
  * __[Dockerfile](Dockerfile.multi)__ : file to build a Docker image of the App

You need to enable __Pipelines__ in the project setting (_Setting -> General -> Visibility_)
and enable __Shared Runners__ and the _dind_ __Specific Runners__ (see below).  
You need to set secret variables (Setting -> CI/CD -> Variables) to allowi default deployment :  
* __DEPLOY\_USER__ : user ID  
* __DEPLOY\_PWD__ : user password  
* __DEPLOY\_IP__ : the server IP to deploy to   

You need to enable __Container registry__ in project setting (_Setting -> General -> Visibility_).  
The __.gitlab-ci.yml__ file no need to be modify (just comment deploy parts that is not needed).  

### The Runners

Two types of Runners is needed:  
* A shared runner using Docker executor (to deploy)  
* A specific runner that use Docker executor with priviliged mode (to build the image)  
> Here the __.gitlab-ci.yml__ use docker:19.03.0 as runner to build the image

Here you have to set your runners or ask GitLab administrator to set them.  

In _Setting -> CI/CD -> Runners_ enable __Shared Runners__ and __Specific Runners__ tag as (dind).  
> For standalone and docker-swarm deployment you need to use a specify runner that mount docker host sockets.

## The .gitlab-ci.yml file

__[.gitlab-ci.yml](.gitlab-ci.yml)__ is the configuration file for the CI/CD pipeline. There are three jobs :  
* __buildImageDocker__ : It builds a docker image of the R Shiny App using the __[Dockerfile](Dockerfile.multi)__ and
records it to the container registry of the project.  
* __retag__ : a fast way to rollback, it retags to _latest_ a commit.  
* __deploy__ : pull the image (tag as _latest_) from the project container registry to a server
(set in secret variables).  

This file can be use as it. But you have to comment the __deploy__ part that you do not need and you may change the server URL too.

### Deployement strategies

There is three deployments strategies in the __.gitlab-ci.yml__ file, you may use just one:  
* __deploy__ : Pull the docker image on a server using secrets credentials.
* __deploy-standalone__: use docker-compose (and [myApp-compose.yml](myApp-compose.yml) ) to create a service (using a specific runner)
* __deploy-swarm__: use docker stack (and [myApp-stack.yml](myApp-stack.yml) to create a stack on a docker swarm cluster using a specific runner. And use traefik V2 as frontal LB in options.  

## Dockerfiles

__[Dockerfile.multi](Dockerfile.multi)__ is the file use to build the docker image for __multi-users__.  
It describes how to create a docker image containing system libraries dependences,
a R Shiny Server, R packages dependencies, files (and directories) of the R Shiny App
and some configurations files.  

__[Dockerfile.single](Dockerfile.single)__ is the file use to build the docker image for __single-user__.  
It describes how to create a docker image containing system libraries dependences,
R packages dependencies, files (and directories) of the R Shiny App and some configurations files.  

__[Dockerfile..simple.renv.rshiny]__ allow to build dynamic container using ARG, for R\_IMAGE, AUTHOR, APP\_NAME, APP\_VERSION and it use Renv to install packages dependencies. See [.gitlab-ci.yml](.gitlab-ci.yml#L28)  

## Test the docker image

If __buildImageDocker__ job __passed__ the docker image should be available in _Packages_ -> _Container Registry_ menu.  
Here you can copy an image name \<_imageName:tag_\> , for example here __gitlab.paca.inra.fr:4567/r-ecosystem/cookbooks/r-shiny-app-ci-cd:latest__ , and run it on you PC.  

To run this R Shiny App :
```sh
sudo docker run -p 8080:3838 --name rshinyapp -d gitlab.paca.inra.fr:4567/r-ecosystem/cookbooks/r-shiny-app-ci-cd:latest
```
Go to [http://localhost:8080](http://localhost:8080)  


For generic project :
```sh
# connect to container registry of you GitLab
# not needed but can by useful
sudo docker login -u <YourUserID> <GitlabRegistryURL>

# Run the image as a container
sudo docker run -p 8080:3838 --name myAwesomeApp -d <ImageName:tag> 
# open you browser to localhost:8080 and Enjoy

sudo docker container stop nyAwesomeApp

# disconnect from the container registry if login
sudo docker logout <GitlabRegistryURL>
```

## Author

[Jean-François Rey](https://jeff.biosp.org) \<jean-francois.rey at inra.fr\>

## License

See [LICENSE](LICENSE)

